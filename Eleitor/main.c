#include <stdio.h>
#include <stdlib.h>

void limpa(float x[5][2]){
    int i = 0, j = 0;
    for (i = 0; i < 7; i++){
        for (j = 0; j < 2; j++){
            x[i][j] = 0;
        }
    }
}

/*void mostra(float x[5][2]){
    int i = 0, j = 0;
    for (i = 0; i < 7; i++){
        for (j = 0; j < 2; j++){
            printf("%.2f ",x[i][j]);
        }
        printf("\n");
    }
}*/

void media(float x[5][2],float c){
    int i = 0;
    for (i = 0; i < 7; i++){
        x[i][1] = (x[i][0]/c)*100;
    }
}

int main()
{
    int i = 0, s = 0;
    float c = 0;
    float x[7][2]; //Matriz dos votos e % dos votos
    limpa(x);
    while(i == 0){
        printf("Digite a escolha do candidato:\n");
        printf("1 - Roberto\n");
        printf("2 - Lucas\n");
        printf("3 - Gorillaz\n");
        printf("4 - Trump\n");
        printf("5 - Daniela\n");
        printf("0 - Nulo\n");
        scanf("%d",&s);
        switch(s){
            case 1:
                printf("Votou em Roberto\n\n");
                x[0][0] = x[0][0] + 1;
                c++;
                break;
            case 2:
                printf("Votou em Lucas\n\n");
                x[1][0] = x[1][0] + 1;
                c++;
                break;
            case 3:
                printf("Votou em Gorillaz\n\n");
                x[2][0] = x[2][0] + 1;
                c++;
                break;
            case 4:
                printf("Votou em Trump\n\n");
                x[3][0] = x[3][0] + 1;
                c++;
                break;
            case 5:
                printf("Votou em Daniela\n\n");
                x[4][0] = x[4][0] + 1;
                c++;
                break;
            case 0:
                printf("Voto Nulo\n\n");
                x[5][0] = x[5][0] + 1;
                c++;
                break;
            case 123: //SAI DO PROGRAMA
                printf("Saindo do programa\n\n\n\n");
                i++;
                break;
            default:
                printf("Voto em branco\n\n");
                x[6][0] = x[6][0] + 1;
                c++;
                break;
        }
    }
    media(x,c);
    printf("Numero de votos totais: %.0f\n",c);
    printf("Roberto ficou com %.0f voto(s), sendo %.2f%% dos votos totais\n",x[0][0],x[0][1]);
    printf("Lucas ficou com %.0f voto(s), sendo %.2f%% dos votos totais\n",x[1][0],x[1][1]);
    printf("Gorillaz ficou com %.0f voto(s), sendo %.2f%% dos votos totais\n",x[2][0],x[2][1]);
    printf("Trump ficou com %.0f voto(s), sendo %.2f%% dos votos totais\n",x[3][0],x[3][1]);
    printf("Daniela ficou com %.0f voto(s), sendo %.2f%% dos votos totais\n",x[4][0],x[4][1]);
    printf("Votos brancos e nulos foram %.0f, sendo %.2f%% dos votos totais\n",x[5][0] + x[6][0],((x[5][0] + x[6][0])/c)*100);

    return 0;
}
